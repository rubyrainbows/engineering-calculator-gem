Engineering Calculator provides basic engineering functions for ruby.

  Gas Dynamics:
    include EngineeringCalculator::GasDynamics
    
    Functions:
      fanno(m,gamma)
      isentropic(m,gamma)
      normal_shock(mx,gamma)
      prandtl_compression(mx,gamma,turning_angle)
      prandtl_expansion(mx,gamma,turning_angle)
      rayleigh(m,gamma)