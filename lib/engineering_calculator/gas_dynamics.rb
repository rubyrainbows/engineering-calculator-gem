require "helpers"

module EngineeringCalculator
  module GasDynamics
    def fanno(m,gamma)
      p_ratio = 1/m * 1/Math.sqrt((2/(gamma+1)*(1+(gamma-1)/2*pow(m, 2))))
     	rho_ratio = 1/m * Math.sqrt((2/(gamma+1)) * (1+(gamma-1)/2*pow(m, 2)))
     	t_ratio = 1/(2/(gamma+1)*(1+(gamma-1)/2*pow(m, 2)))
     	u_ratio = m * 1/Math.sqrt(2/(gamma+1) * (1 + (gamma-1)/2*pow(m, 2)))
     	po_ratio = 1/m * pow(2/(gamma+1) * (1+(gamma-1)/2*pow(m, 2)),(gamma+1)/(gamma-1)/2)
     	fanno_param = (1-pow(m, 2))/(gamma*pow(m, 2)) + (gamma+1)/(gamma*2)*Math.log(pow(m, 2)/(2/(gamma+1)*(1+(gamma-1)/2*pow(m, 2))))
    	result = {
    	    :p_ratio => p_ratio,
    	    :rho_ratio => rho_ratio,
    	    :t_ratio => t_ratio,
    	    :u_ratio => u_ratio,
    	    :po_ratio => po_ratio,
    	    :fanno_param => fanno_param
    	  }
    	  return result
    end
    def isentropic(m,gamma)
      ratio_t = pow(1 + (gamma - 1)/2 * pow(m, 2), -1)
      ratio_p = pow(1 + (gamma - 1)/2 * pow(m, 2), -gamma/(gamma-1))
      ratio_rho = pow(1 + (gamma - 1)/2 * pow(m, 2), -1/(gamma-1))
      ratio_a = pow((gamma + 1)/2, -((gamma + 1)/(gamma - 1)/2))/m * pow(1 + (gamma - 1)/2 * pow(m, 2), ((gamma + 1)/(gamma - 1))/2)
      result = {
        :ratio_t => ratio_t,
        :ratio_p => ratio_p,
        :ratio_rho => ratio_rho,
        :ratio_a => ratio_a
      }
      return result
    end
    def normal_shock(mx,gamma)
     	my = Math.sqrt((pow(mx,2)*(gamma-1)+2)/(2*gamma*pow(mx,2)-(gamma-1)))
    	py_px = 2*gamma* pow(mx,2) /(gamma+1)-(gamma-1)/(gamma+1)
    	rhoy_rhox = (gamma+1)*pow(mx,2)/((gamma-1)*pow(mx,2)+2)
    	ty_tx = (1 + (gamma - 1)/2 * pow(mx, 2))*(2*gamma/(gamma - 1) * pow(mx, 2) - 1)/(pow(mx, 2)*(2*gamma/(gamma - 1) + (gamma - 1)/2))
    	poy_pox = pow((gamma + 1)/2 * pow(mx, 2)/(1 + (gamma - 1)/2 * pow(mx, 2)), gamma/(gamma - 1)) * pow(1/(2 * gamma/(gamma+1) * pow(mx,2) - (gamma-1)/(gamma+1)), 1/(gamma - 1));
      result = {
    	  :my => my,
    	  :py_px => py_px,
    	  :rhoy_rhox => rhoy_rhox,
    	  :ty_tx => ty_tx,
    	  :poy_pox => poy_pox
      }
      return result
    end
    def oblique(mx,gamma,delta)
     	pi = Math::PI
      # Initial guess that beta and delta coincide.
     	beta = delta*pi/180
     	e = 1
     	rhs = Math.tan(delta*pi/180)

     	while (e >= 1*10**(-5))
     		lhs = 2*(1/Math.tan(beta))*(pow(mx,2)*pow(Math.sin(beta),2)-1)/(pow(mx,2)*(gamma+Math.cos(2*beta))+2)
     		e = rhs - lhs
     		beta = beta + 0.00001
     	end
     	ratio_rho = (gamma+1) * pow(mx, 2)*pow(Math.sin(beta), 2) / ((gamma-1)*pow(mx, 2)*pow(Math.sin(beta), 2)+2)
     	beta = beta*180/pi;

     	ratio_p = 1+2*gamma/(gamma+1)*(pow(mx,2)*pow(Math.sin(beta*pi/180),2)-1);
     	ratio_t = ratio_p*pow((gamma+1)*pow(mx,2)*pow(Math.sin(beta*pi/180),2)/((gamma-1)*pow(mx,2)*pow(Math.sin(beta*pi/180),2)+2),-1);
     	my	= (1/Math.sin((beta-delta)*pi/180))*pow((1+0.5*(gamma-1)*pow(mx,2)*pow(Math.sin(beta*pi/180),2))/(gamma*pow(mx,2)*pow(Math.sin(beta*pi/180),2)-0.5*(gamma-1)),0.5);
     	ratio_px = pow(1 + (gamma - 1)/2 * pow(mx, 2), -gamma/(gamma-1));
     	ratio_py = pow(1 + (gamma - 1)/2 * pow(my, 2), -gamma/(gamma-1));
     	ratio_po = ratio_px/ratio_py*ratio_p;
     	result = {
     	  :my => my,
     	  :ratio_rho => ratio_rho,
     	  :beta => beta,
     	  :ratio_p => ratio_p,
     	  :ratio_t => ratio_t,
     	  :ratio_po => ratio_po
     	}
     	return result
    end
    def prandtl_compression(mx,gamma,turning_angle)
      pi = Math::PI
     	nux = Math.sqrt((gamma+1)/(gamma-1)) * Math.atan(Math.sqrt((gamma-1)/(gamma+1)*(pow(mx, 2)-1))) - Math.atan(Math.sqrt(pow(mx, 2)-1))
     	turningAngle = turning_angle*pi/180
     	nuy = nux - turningAngle
     	my = 1
     	e = 1
     	while (e >= 0.00001)
     		nuy_test = Math.sqrt((gamma+1)/(gamma-1)) * Math.atan(Math.sqrt((gamma-1)/(gamma+1)*(pow(my, 2)-1))) - Math.atan(Math.sqrt(pow(my, 2)-1))
     		e = nuy - nuy_test
     		my = my+0.00001
     	end
     	my = my-0.00001
     	ty_tx = (1+(gamma-1)/2*pow(mx, 2))/(1+(gamma-1)/2*pow(my, 2))
     	py_px = pow((1+(gamma-1)/2*pow(mx, 2))/(1+(gamma-1)/2*pow(my, 2)), gamma/(gamma-1))
     	rhoy_rhox = pow((1+(gamma-1)/2*pow(mx, 2))/(1+(gamma-1)/2*pow(my, 2)), 1/(gamma-1))
     	mux = Math.asin(1/mx)
     	muy = Math.asin(1/my)
     	result = {
     	  :ty_tx => ty_tx,
     	  :py_px => py_px,
     	  :rhoy_rhox => rhoy_rhox,
     	  :my => my,
     	  :nux => nux*180/pi,
     	  :nuy => nuy*180/pi,
     	  :mux => mux*180/pi,
     	  :muy => muy*180/pi,
     	}
     	return result
    end
    def prandtl_expansion(mx,gamma,turning_angle)
      pi = Math::PI
     	nux = Math.sqrt((gamma+1)/(gamma-1)) * Math.atan(Math.sqrt((gamma-1)/(gamma+1)*(pow(mx, 2)-1))) - Math.atan(Math.sqrt(pow(mx, 2)-1))
     	turningAngle = turning_angle*pi/180
     	nuy = nux + turningAngle
     	my = 1
     	e = 1
     	while (e >= 0.00001)
     		nuy_test = Math.sqrt((gamma+1)/(gamma-1)) * Math.atan(Math.sqrt((gamma-1)/(gamma+1)*(pow(my, 2)-1))) - Math.atan(Math.sqrt(pow(my, 2)-1))
     		e = nuy - nuy_test
     		my = my+0.00001
     	end
     	my = my-0.00001
     	ty_tx = (1+(gamma-1)/2*pow(mx, 2))/(1+(gamma-1)/2*pow(my, 2))
     	py_px = pow((1+(gamma-1)/2*pow(mx, 2))/(1+(gamma-1)/2*pow(my, 2)), gamma/(gamma-1))
     	rhoy_rhox = pow((1+(gamma-1)/2*pow(mx, 2))/(1+(gamma-1)/2*pow(my, 2)), 1/(gamma-1))
     	mux = Math.asin(1/mx)
     	muy = Math.asin(1/my)
     	result = {
     	  :ty_tx => ty_tx,
     	  :py_px => py_px,
     	  :rhoy_rhox => rhoy_rhox,
     	  :my => my,
     	  :nux => nux*180/pi,
     	  :nuy => nuy*180/pi,
     	  :mux => mux*180/pi,
     	  :muy => muy*180/pi,
     	}
     	return result
    end
    def rayleigh(m,gamma)
     	p_ratio = (gamma+1)/(1+gamma*pow(m, 2));
     	rho_ratio = (1+gamma*pow(m,2))/((1+gamma)*pow(m,2));
     	t_ratio = pow(gamma+1, 2)*pow(m, 2)/pow(1+gamma*pow(m, 2), 2);
     	u_ratio = (gamma+1)*pow(m, 2)/(1+gamma*pow(m, 2));
     	po_ratio = (gamma+1)/(1+gamma*pow(m, 2)) * pow(2/(gamma+1)*(1+(gamma-1)/2*pow(m, 2)), gamma/(gamma-1));
     	to_ratio= 2*(gamma+1)*pow(m, 2)/pow(1+gamma*pow(m, 2),2) * (1+(gamma-1)/2*pow(m, 2));
      result = {
        :p_ratio => p_ratio,
        :rho_ratio => rho_ratio,
        :t_ratio => t_ratio,
        :u_ratio => u_ratio,
        :po_ratio => po_ratio,
        :to_ratio => to_ratio
      }
      return result
    end
  end
end