# -*- encoding: utf-8 -*-
require File.expand_path('../lib/engineering_calculator/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Thomas Muntaner"]
  gem.email         = ["thomas.muntaner@gmail.com"]
  gem.description   = %q{Provides engineering functions for ruby}
  gem.summary       = %q{Engineering Calculator}
  gem.homepage      = "http://www.engineering-calculator.com"

  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.name          = "engineering_calculator"
  gem.require_paths = ["lib"]
  gem.version       = EngineeringCalculator::VERSION
end
